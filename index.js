// console.log("Hello World!")

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// [SECTION] Discussion 1 

// Using "keyup" event
/*
	txtFirstName.addEventListener("keyup", (event) => {
		spanFullName.innerHTML = txtFirstName.value;
	});

	txtFirstName.addEventListener("keyup", (event) => {
		// the document where our JS is connected
		console.log(event.target)
		// the value that our "keyup" event listened
		console.log(event.target.value)
	});
*/

// [SECTION] Discussion 2 

const updateFullName = () => {

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);